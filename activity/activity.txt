What is an alternative term for pinging?
Answer: Echo Request

What is the term used when a server cannot be pinged?
Answer: down

A security setting that limits the communication of systems from outside the server.
Answer: Outbound Rules

A security setting that limits the communication of systems from within the server.
Answer: Inbound Rules

From which tab in the instance information summary can we change the firewall rules of the instance?
Answer: Security tab

ICMP stands for?
Answer: Internet Control Message Protocol => Error handler

TCP stands for?
Answer: Transmission Control Protocol => a communication standard used to send packet datas across the network

Which protocol does a ping request use?
Answer: ICPM

What is the IP address that indicates access from anywhere?
Answer: 0.0.0.0/0

A command that can be used to look on various usage metrics for a given server.
Answer: htop

Processor or memory __________ means the amount of resources needed to keep a given task or process running smoothly within the operating system.
Answer: Usage

A command that can be used to determine disk storage usage.
Answer: df

From which tab in the instance information summary can we view the usage of an instance without directly accessing the instance?
Answer: Monitoring tab

A command that can be used to edit the contents of a file.
Answer: nano

A command used to host a Node.js app.
Answer: npx serve

Which protocol is used to enable access to a Node.js app on a given port?
Answer: TCP